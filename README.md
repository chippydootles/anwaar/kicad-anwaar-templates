# KiCad-Anwaar-Templates

This repository contains templates to help design your own
face plates for chippydootles' Anwaar families.

Pendant and Slab families are still under development and are subject to change. The Amulet family is pre-release but should be ready.

To use either clone this to your kicad user templates folder or just move the folders from a downloaded zip into the directory.
