The most fun about these designs is the actual art work. 
There's a couple ways to go about it but I'll surmise the workflow.

Planning:
Think about something meaningful to you (or fun, cool, stupid),
something you'd want to have a literal dc current path through
in some way or another. Once you have that idea think of a circuit.
Simplest case for these is one involving led's. Once you have that
in mind you can start designing. I like to doodle to try to plan it out.

Simple designs:
You can either plot your board as an svg and import that into the 
art program your choice, or go ahead and use the provided krita file
in the Art folder of the project. I do my work with a pen tablet on 
a raster layer of krita above the outlines of the board, then export 
the desired layer as a .BMP bitmap image in order to use kicad's built in 
bitmap to footprint tool.

Complex designs:
Before moving on to the actual drawing of the copper design, 
it may help to acutally dive into kicad and layout your components.
Especially so if you want to hide vias or incorporate jumper traces in your design,
it helps to do the board layout. Go as far as routing the board as well, keep in mind
if you plan on any part of the circuit being replaced with the art itself to give yourself 
some clearance for margin of error in the layout. Once you have the circuit done,
export the pcb as an svg to use in the art program of your choice. The difference here is
to make sure to include the copper layers which you will use as reference points for your design.
I.e see where traces connect to and where vias lead, allowing you to hand draw those a bit more 
easy with having the visual reference of your routing.

art notes:
work in a Grayscale color space, if you have an rgb color space your export may lose information. 
	(such asrich blacks of rgb will be faded 
	leading to a lower resolution bitmap)

if using SVG I import files to my art program as 1200dpi make sure to match this in the later bitmap to footprint tool.

it can be hard to keep track of trace tolerences while drawing, 
	in this case make sure to have a good reference for the mils/mm clearances 
	that your boardhouse needs
	so you can avoid bridging artwork copper.

exporting to BMP: 
Make sure only the art layer gets exported. In krita i do this by visually hiding all other layers.
I draw white over black for the design. Remember you're importing a BMP which is basically just pixel on or off.

Importing to kicad footprint:
Along with PCBnew and EEschema tools in kicad (the pcb design and the schematic design tools) there's a few handy other ones.
one of these is the image importer and this is where we convert our bitmap into a kicad usable footprint.
Simply load your bitmap, make sure if using the DPI option to put in the resolution you exported your bitmap at.
(in my case 1200DPI), choose weather you want negative or positive (this reverses the black and white solids of the image),
and chose what layer you want to chose. Notice here there is no copper layer, this adds an extra manual step for us but isn't the end of the world.
If you plan on having the copper exposed with no mask covering it I suggest chooing the solder mask layer.

assign footprint to graphic placeholder and launch and update the pcb.
at this point you can either be happy with the artwork or redraw/fix things and re-export/re-import.
If you're happy with the artwork it's time for the next step of turning that babby into a copper layer.

Moding to copper layer:
As noted the image tool doesn't actually allow you to create a copper layer from the export. 
This makes sense for certain reasons but for our work case we throw this to the wind! You will
have to edit the exported footprint in a text editor and find any reference to the 
chosen export layer (F.Mask) and replace it with desired layer(F.Cu).
after that you can reload kicad and the part should now be ready to go.

Adding a mask layer:

