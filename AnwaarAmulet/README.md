# Anwaar Amulet Face Plate Template

This project serves as a KiCad template for creating your own amulet face plate!

Template is a work in progress but should be functional!

## Usage
To use this template simply:
* copy the 'AnwaarAmulet' folder into you KiCad user templates folder
* From KiCad File > New Project from Template > User Templates > Anwaar Amulet Face Plate
* Select OK and name your project
* start designing!

More information about anwaar can be found on
[!chippydootles.com]('https://chippydootles.com')


## About

Anwaar Amulets are two seperate circuit boards mounted together.

These amulets are meant to promote artistic use of printed circuit board design. 
It is also a way to quite literally have power flow through symbols,
and this was the basis that I started with! In my amulet designs I try to incorporate
the artistic design into copper which will be part of a simple circuit. In this case an led circuit.
I tried my best to design them modular so  that only one base board can be used with many different designs.

the AnwaarAmulet template is provided to enable the design of a face plate compatible with the Amulet base board.

Folders in template:
 * Art - contains files for artistic design
 * Docs - info and how to
 * GBR - folder for plotted gerber output

[!Basic Design info]('https://chippydootles.com/anwaar/anwaar-guide/')

## Sharing your project

>I made anwaar to see what others can do with it,
>If you end up making your own I want to see!
>
>If inclined, email me: chipperdoodles@protonmail.com
>
>Share pictures so I can put you own my websites showcase!
>If you do be sure to include:
>- a project name (or placeholder for me to call it)
>- a name or handle you want to be referred to as
>- pronouns (just helpful for me referring to you in the post)
>- and optionally one link you'd want to people to find you at. 
>  (i.e social media handle or personal blog site)
>- Optionally a brief summary if you want to share background on it
>  (made it for a friend, thought it would be cool, had this neat idea)


##Version History

1.0: 8/13/21
 * Initial Rlease
 
1.1: 8/10/23
 * Updated language and simplified readme

1.2: 8/29/23
 * updated for version 3.0 of base board
 
1.3: 04/21/24
 * minor fixes
 * Usb Keepout drawing/zone added
 * Runtime estimate section added
 * editors note on standoffs added
